<!DOCTYPE html>
<html>
<head>
    <script src="jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $("p").click(function(){
                $(this).delay(50).fadeIn(slow);
                $(this).delay(50).fadeOut(slow);
                $(this).delay(50).fadeIn(slow);
                $(this).delay(50).fadeOut(slow);
            });
        });
    </script>
</head>
<body>

<p>If you click on me, I will disappear.</p>
<p>Click me away!</p>
<p>Click me too!</p>

</body>
</html>